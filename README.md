# Vuerate - An easy-to-use rating plugin made with Vue.js

### Usage Example:
```HTML
<rating :score='3'></rating>
```
**Note:** This plugin is designed to work with whole numbers. A rating having a decimal number will be rounded off to the nearest lower whole number.